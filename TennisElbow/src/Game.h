#pragma once

#include <GameEngine.h>
#include <SDL_gamecontroller.h>

struct SDL_Renderer;

class ScreenManager;

#define NEW_SYSTEM

class Game : public GameEngine
{
    friend class GameEngine;

public:
    ~Game();

protected:
    Game();

    void InitializeImpl() override;
    void UpdateImpl(const float& dt) override;
    void DrawImpl(SDL_Renderer* renderer, const float& dt) override;

    SDL_GameController* _controller = nullptr;
    ScreenManager* _screenManager = nullptr;
};
