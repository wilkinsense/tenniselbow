#include "Player.h"

#include <iostream>

#include <SDL.h>
#include <SDL_gamecontroller.h>
#include <SDL_image.h>

#include "GameConstants.h"

Player::Player() 
    : Player("res/me.png", 
             PLAYER_SPEED)
{
}

Player::Player(std::string imageFilename, float speed)
    : GameObject()
    , _playerImageFile(imageFilename)
    , _speed(speed)
{
}

Player::~Player()
{
    SDL_DestroyTexture(_playerImage);
    _controller = nullptr;
}

void Player::Initialize(SDL_Renderer* renderer)
{
    _playerImage = IMG_LoadTexture(renderer, _playerImageFile.c_str());
    SDL_QueryTexture(_playerImage, nullptr, nullptr, &_width, &_height);
}

void Player::Update(const SDL_Event& evt, const float& dt)
{
    _transform.position += _movement;
}

const Vector2& Player::GetMovement() const
{
    return _movement;
}

void Player::Draw(SDL_Renderer* renderer, const float& dt)
{
    Vector2 centerOffset((GetWidth() / 2), (GetHeight() / 2));
    SDL_Rect location = { (int)(_transform.position.x - centerOffset.x), (int)(_transform.position.y - centerOffset.y), GetWidth(), GetHeight() };
    SDL_RenderCopy(renderer, _playerImage, nullptr, &location);
}

const int Player::GetWidth() const
{
    return _width;
}

const int Player::GetHeight() const
{
    return _height;
}
