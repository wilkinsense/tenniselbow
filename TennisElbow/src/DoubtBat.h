#pragma once

#include <memory>
#include "Player.h"

struct SDL_Texture;

class DoubtBat : public Player
{
public:
    DoubtBat();
    virtual ~DoubtBat();

    void Update(const SDL_Event& evt, const float& dt) override;

    void SetTarget(std::shared_ptr<GameObject> target);

private:
    std::shared_ptr<GameObject> _target;
    Vector2 _direction = Vector2::Zero;
    bool _followingTarget = true;
};
