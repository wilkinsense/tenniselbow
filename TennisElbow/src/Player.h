#pragma once

#include <string>

#include <SDL_GameController.h>

#include "GameObject.h"

struct SDL_Texture;

class Player : public GameObject
{
public:
    Player();
    Player(std::string imageFilename, float speed);
    virtual ~Player();

    void Initialize(SDL_Renderer* renderer) override;

    void Update(const SDL_Event& evt, const float& dt) override;
    void Draw(SDL_Renderer* renderer, const float& dt) override;

    const int GetWidth() const;
    const int GetHeight() const;
    const Vector2& GetMovement() const;

protected:
    std::string _playerImageFile;
    SDL_Texture* _playerImage = nullptr;
    SDL_GameController* _controller = nullptr;

    float _speed = 0.0f;
    int _width = 0, _height = 0;
    Vector2 _movement;
};
