#include "TennisScreen.h"

#include <algorithm>
#include <iostream>
#include <math.h>

#include <SDL.h>
#include <SDL_image.h>

#include <GameUtils.h>
#include <MathConstants.h>
#include <ScreenManager.h>

#include "GameConstants.h"

#include "Ball.h"
#include "DoubtBat.h"
#include "HumanPlayer.h"
#include "Net.h"
#include "Opponent.h"

/*static*/ const Vector3 TennisScreen::c_UpperCourtStartingPosition = { 100.0f, 125.0f, 0.0f };
/*static*/ const Vector3 TennisScreen::c_LowerCourtStartingPosition = { 30.0f, 15.0f, 0.0f };
/*static*/ const Vector3 TennisScreen::c_NetPosition = { 69.0f, 71.0f, 0.0f };

TennisScreen::TennisScreen(SDL_GameController* controller)
    : _controller(controller)
    , _hitPosition(Vector3::Zero)
    , _hitVelocity(Vector3::Zero)
    , _servingDirection(Vector2::Zero)
    , _hitNet(false)
    , _bounces(0)
    , _roundState(RoundState::SERVING)
    , _gameState(GameState::PLAY)
    , _fogAmount(0.0f)
    , _fogDirection(1.0f)
    , _fadeBlendMode(SDL_BLENDMODE_NONE)
    , _playerMovementBlend(0.0f)
    , _ballMovementBlend(0.0f)
    , _fadeSurface(nullptr)
    , _fadeImage(nullptr)
    , _courtImage(nullptr)
    , _screenWidth(0)
    , _screenHeight(0)
    , _timeScale(1.0f)
    , _player(std::make_shared<HumanPlayer>())
    , _opponent(std::make_shared<Opponent>())
    , _net(std::make_shared<Net>())
    , _ball(std::make_shared<Ball>())
{ }

TennisScreen::~TennisScreen()
{
    SDL_DestroyTexture(_courtImage);
    SDL_DestroyTexture(_fadeImage);
    SDL_DestroyTexture(_fadeSurface);
}

void TennisScreen::InitializeImpl(SDL_Renderer* renderer)
{
    _courtImage = IMG_LoadTexture(renderer, "res/court.png");
    _fadeImage = IMG_LoadTexture(renderer, "res/fade.png");

    SDL_GetWindowSize(ScreenManager::GetInstance()->GetWindow(), &_screenWidth, &_screenHeight);
    SDL_RenderSetScale(renderer, SCALE_FACTOR, SCALE_FACTOR);

    _objects.push_back(_net);
    _objects.push_back(_player);
    _objects.push_back(_opponent);
    _objects.push_back(_ball);

    for (int batIndex = 0; batIndex < DOUBT_BATS_COUNT; batIndex++)
    {
        std::shared_ptr<DoubtBat> bat = std::make_shared<DoubtBat>();
        bat->SetTarget(_ball);

        float randomX = rand() % 2 == 0 ? 0.0f : (float)_screenWidth / SCALE_FACTOR;
        float randomY = rand() % 2 == 0 ? 0.0f : (float)_screenHeight / SCALE_FACTOR;
        bat->GetTransform().position = { randomX, randomY, 1.0f };

        _objects.push_back(bat);
    }

    _player->SetController(_controller);

    _net->GetTransform().position = c_NetPosition;

    std::for_each(_objects.begin(), _objects.end(), [renderer](std::shared_ptr<GameObject> obj)
    {
        obj->Initialize(renderer);
    });

    _fadeSurface = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, _screenWidth / SCALE_FACTOR, _screenHeight / SCALE_FACTOR);

    _fadeBlendMode = SDL_ComposeCustomBlendMode(
        SDL_BLENDFACTOR_DST_COLOR,
        SDL_BLENDFACTOR_ZERO,
        SDL_BLENDOPERATION_ADD,
        SDL_BLENDFACTOR_DST_ALPHA,
        SDL_BLENDFACTOR_ZERO,
        SDL_BLENDOPERATION_ADD);
    int result = SDL_SetTextureBlendMode(_fadeImage, _fadeBlendMode);
    SDL_SetTextureBlendMode(_fadeSurface, SDL_BLENDMODE_BLEND);

    AddFoggedObject(
        _player,
        [&player = _player]() -> bool
        { return player->GetMovement().Magnitude() > 0.0f; },
        0.75f,
        1.0f);

    AddFoggedObject(
        _ball,
        [&ball = _ball]() -> bool
        { return ball->GetVelocity().Magnitude() > 0.0f; },
        0.125f,
        0.5f);

    AddFoggedObject(
        _opponent,
        [&opponent = _opponent]() -> bool
        { return opponent->GetMovement().Magnitude() > 0.0f; },
        0.75f,
        1.0f);

    Reset();

    // Let Replay serve values
    /*_hitPosition = { 105.0f, 130.0f, 0.745036876f };
    _hitVelocity = { 0.0f, 0.0f, -3.49657798f };*/

    //_hitPosition = { 105.0f, 130.0f, 1.27140641f };
    //_hitVelocity = { 0.0f, 0.0f, 0.0608121306f };
}

void TennisScreen::Reset()
{
    // Using the default member-wise initializer for our new struct.
    _servingDirection = Vector2::Zero;

    _roundState = RoundState::SERVING;
    _gameState = GameState::PLAY;

    _hitNet = false;
    _bounces = 0;

    // Default positions for player and opponent; this will have to change once rounds are implemented.
    _player->GetTransform().position = c_LowerCourtStartingPosition;
    _opponent->GetTransform().position = c_UpperCourtStartingPosition;

    _ball->Reset();
    _opponent->Reset();
    _impactPoints.clear();

    _fogAmount = 0.0f;

    system("cls");

    std::for_each(_foggedObjects.begin(),
                  _foggedObjects.end(),
                  [](std::pair<std::shared_ptr<GameObject> const, FoggedEntry>& pair)
    {
        FoggedEntry& entry = pair.second;
        entry.currentFogAmount = entry.minimumFogAmount;
    });
}

void TennisScreen::Update(const SDL_Event& e, float dt)
{
    HandleInput(e);

    dt *= _timeScale;

    if (dt > FIXED_TIMESTEP)
    {
        dt = FIXED_TIMESTEP;
    }

    _fogAmount = MathUtils::Clamp(_fogAmount + dt * _fogDirection, 0.0f, FOG_FADE_TOTAL_TIME);
    if (_fogAmount <= 0.0f
        || _fogAmount >= FOG_FADE_TOTAL_TIME)
    {
        _fogAmount += (MathUtils::Clamp(_fogAmount, 0.0f, FOG_FADE_TOTAL_TIME) - _fogAmount);
        _fogDirection *= -1.0f;
    }

    if (_gameState == GameState::PLAY)
    {
        std::for_each(_objects.begin(),
                      _objects.end(),
                      [e, dt](std::shared_ptr<GameObject> obj)
        {
            obj->Update(e, dt);
        });

        if (_ball->IsBouncing())
        {
            _impactPoints.push_back(_ball->GetTransform().position);
        }

        if (_roundState == RoundState::PLAY)
        {
            if (_ball->IsBouncing())
            {
                _bounces++;
                _opponent->CalculateBallImpact(_ball);
            }

            if (IsRoundOver())
            {
                _roundState = RoundState::END;
                _opponent->Reset();
            }
        }
    }
}

void TennisScreen::HandleInput(const SDL_Event& evt)
{
    bool buttonUp = evt.type == SDL_CONTROLLERBUTTONUP;
    bool keyUp = evt.type == SDL_KEYUP;

    bool buttonReleased = buttonUp || keyUp;
    Uint8 button = evt.cbutton.button;
    SDL_Keycode key = evt.key.keysym.sym;

    if (_roundState == RoundState::SERVING)
    {
        _servingDirection = { (SERVING_DIRECTION_OFFSET_X - _player->GetTransform().position.x) * 2,
                             (_opponent->GetTransform().position.y - _player->GetTransform().position.y) };

        _ball->GetTransform().position.x = _player->GetTransform().position.x + BALL_SERVING_OFFSET_X;
        _ball->GetTransform().position.y = _player->GetTransform().position.y + BALL_SERVING_OFFSET_Y;
        if (buttonReleased)
        {
            bool replay = (buttonUp && button == SDL_CONTROLLER_BUTTON_B) || (keyUp && key == SDLK_b);
            if (replay)
            {
                _ball->Reset();
                //_ball->TEST_SetVelocity(_hitVelocity);
                _ball->GetTransform().position = _hitPosition;
            }

            GameUtils::Log("Button pressed");

            bool isServing = (buttonUp && button == SDL_CONTROLLER_BUTTON_A) || (keyUp && key == SDLK_SPACE);

            if (isServing || replay)
            {
                GameUtils::Log("We're serving!");

                Vector3 difference = { 0.0f, 0.0f, 0.0f };
                if (_ball->IsOnGround())
                {
                    GameUtils::Log("Ball is being tossed!");
                    difference = { 0.0f, 0.0f, BALL_THROW_HEIGHT };
                }
                else
                {
                    GameUtils::Log("Hitting ball!");
                    _roundState = RoundState::PLAY;
                    _ball->SetActive(true);
                    float velocityZ = -PHYSICS_GRAVITY * ((_ball->GetVelocity().z < 0.0f) ? 0.75f : 0.33f);

                    float hyp = sqrtf(powf(_servingDirection.x, 2) + powf(_servingDirection.y, 2));
                    float magX = _servingDirection.x / hyp;
                    float magY = _servingDirection.y / hyp;
                    Vector2 normalizedServingDirection = MathUtils::Normalize(_servingDirection);
                    difference = { SERVE_STRENGTH * normalizedServingDirection.x,
                                  SERVE_STRENGTH * normalizedServingDirection.y,
                                  velocityZ };

                    _hitPosition = _ball->GetTransform().position;
                    _hitVelocity = _ball->GetVelocity();
                }

                _ball->ApplyForce(difference);

                if (_roundState == RoundState::PLAY)
                {
                    _opponent->CalculateBallImpact(_ball);
                }
            }
        }
    }
    else if (_roundState == RoundState::PLAY)
    {
        SDL_Rect ballRect, shadowRect;
        _ball->GetDrawRect(&ballRect);
        _ball->GetShadowDrawRect(&shadowRect);

        SDL_Rect netRect;
        _net->GetDrawRect(&netRect);

        Vector3 velocity = _ball->GetVelocity();
        Vector3 normalizedVelocity = { velocity.x, velocity.y, velocity.z * Z_HEIGHT_CORRECTION };
        normalizedVelocity = MathUtils::Normalize(normalizedVelocity);

        bool ballIntersection = SDL_HasIntersection(&ballRect, &netRect);
        bool shadowIntersection = SDL_HasIntersection(&shadowRect, &netRect);
        float difference = (_net->GetTransform().position.y + (netRect.h / 2.0f)) - (_ball->GetTransform().position.y - (_ball->GetTransform().position.z * Z_HEIGHT_CORRECTION));
        float correctedDifference = difference + normalizedVelocity.z;

        GameUtils::Log("Corrected difference: (%f, %f)", _ball->GetTransform().position.y, _net->GetTransform().position.y);

        bool hitNetTest = correctedDifference <= NET_HEIGHT_THRESHOLD;
        bool letTest = correctedDifference < LET_ZONE_THRESHOLD;

        if (ballIntersection && shadowIntersection && !_hitNet)
        {
            if ((_ball->GetTransform().position.y - _net->GetTransform().position.y) < -0.5f)
            {
                hitNetTest = true;
                GameUtils::Log("womp %f", difference);
            }

            _hitNet = hitNetTest || letTest;

            if (hitNetTest)
            {
                velocity.x *= NET_HIT_BOUNCE_DECAY;
                velocity.y *= NET_HIT_BOUNCE_DECAY;
                velocity.z = 0.0f;
                //_roundState = ROUND_STATE_END;

                _ball->ApplyForce(velocity);
            }
            else if (letTest)
            {
                velocity.x *= LET_HIT_BOUNCE_DECAY_XY;
                velocity.y *= LET_HIT_BOUNCE_DECAY_XY;
                velocity.z *= LET_HIT_BOUNCE_DECAY_Z;

                _ball->ApplyForce(velocity);
                _impactPoints.push_back(_ball->GetTransform().position);
            }
        }
    }

    if (buttonReleased && button == SDL_CONTROLLER_BUTTON_X || key == SDLK_x)
    {
        Reset();
    }
}

void TennisScreen::Draw(SDL_Renderer* renderer, float dt)
{
    // Set the draw colour for our point.
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);

    SDL_Rect courtRect = { 0, 0 };
    SDL_QueryTexture(_courtImage, nullptr, nullptr, &courtRect.w, &courtRect.h);

    SDL_RenderCopy(renderer, _courtImage, NULL, &courtRect);

    std::vector<std::shared_ptr<GameObject>> renderOrder;
    CalculateDrawOrder(renderOrder);

    std::for_each(_objects.begin(),
                  _objects.end(),
                  [renderer, dt](std::shared_ptr<GameObject>& obj)
    {
        obj->Draw(renderer, dt);
    });

    if (_roundState == RoundState::SERVING)
    {
        Uint8 r, g, b, a;
        SDL_GetRenderDrawColor(renderer, &r, &g, &b, &a);
        SDL_SetRenderDrawColor(renderer, 0xFF, 0, 0, SDL_ALPHA_OPAQUE);

        SDL_RenderDrawLine(renderer, (int)(_player->GetTransform().position.x), (int)(_player->GetTransform().position.y),
                           (int)(_player->GetTransform().position.x + _servingDirection.x), (int)(_player->GetTransform().position.y + _servingDirection.y));

        SDL_SetRenderDrawColor(renderer, r, g, b, a);
    }
    else if (!_impactPoints.empty())
    {
        Uint8 r, g, b, a;
        SDL_GetRenderDrawColor(renderer, &r, &g, &b, &a);
        SDL_SetRenderDrawColor(renderer, 0, 0xFF, 0, SDL_ALPHA_OPAQUE);

        for (int pointIndex = 0; pointIndex < _impactPoints.size(); pointIndex++)
        {
            Vector3 impactPoint = _impactPoints[pointIndex];

            SDL_Rect impactLocation = { (int)impactPoint.x - (5), (int)impactPoint.y - (5), 10, 10 };
            SDL_RenderDrawRect(renderer, &impactLocation);
        }

        SDL_SetRenderDrawColor(renderer, r, g, b, a);
    }

    DrawFoggedObjects(renderer, dt);
}

void TennisScreen::DrawFoggedObjects(SDL_Renderer* renderer, const float& dt)
{
    SDL_Texture* previousTarget = SDL_GetRenderTarget(renderer);
    int result = SDL_SetRenderTarget(renderer, _fadeSurface);

    Uint8 r, g, b, a;
    SDL_GetRenderDrawColor(renderer, &r, &g, &b, &a);

    Uint8 fogThickness = (Uint8)((_fogAmount / FOG_FADE_TOTAL_TIME) * 0xFF);
    SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, fogThickness);
    SDL_RenderClear(renderer);

    SDL_Rect fadeRect = { 0, 0 };
    result = SDL_QueryTexture(_fadeImage, nullptr, nullptr, &fadeRect.w, &fadeRect.h);

    std::for_each(_foggedObjects.begin(),
                  _foggedObjects.end(),
                  [dt, fadeRect, &result, renderer, this](std::pair<std::shared_ptr<GameObject> const, FoggedEntry>& pair)
    {
        FoggedEntry& entry = pair.second;
        float deltaFactor = dt * FOG_FADE_BREATH_FACTOR;

        bool isMoving = (entry.movement)();
        float fogDelta = entry.currentFogAmount + (isMoving ? 1.0f : -1.0f) * deltaFactor;

        entry.currentFogAmount = MathUtils::Clamp(fogDelta,
                                                  entry.minimumFogAmount,
                                                  entry.maximumFogAmount);

        Sint16 fadeWidth = (Sint16)(fadeRect.w * entry.currentFogAmount);
        Sint16 fadeHeight = (Sint16)(fadeRect.h * entry.currentFogAmount);

        SDL_Rect fadeLocation = {
            (Sint16)(entry.target->GetTransform().position.x - fadeWidth / 2),
            (Sint16)(entry.target->GetTransform().position.y - (entry.target->GetTransform().position.z * Z_HEIGHT_CORRECTION) - fadeHeight / 2),
            fadeWidth,
            fadeHeight };
        result = SDL_RenderCopy(renderer, _fadeImage, nullptr, &fadeLocation);
    });

    /*if (_player->GetMovement().Magnitude() > 0.0f)
    {
        _playerMovementBlend += dt * FADE_BREATH_FACTOR;
    }
    else
    {
        _playerMovementBlend -= dt * FADE_BREATH_FACTOR;
    }

    _playerMovementBlend = MathUtils::Clamp(_playerMovementBlend, 0.0f, 1.0f);

    if (!_ball->IsOnGround())
    {
        _ballMovementBlend += dt * FADE_BREATH_FACTOR;
    }
    else
    {
        _ballMovementBlend -= dt * FADE_BREATH_FACTOR;
    }

    _ballMovementBlend = MathUtils::Clamp(_ballMovementBlend, 0.0f, 1.0f);*/

    float scaleMod = (_playerMovementBlend * 0.25f) + 0.55f;
    Sint16 fadeWidth = (Sint16)(fadeRect.w * scaleMod);
    Sint16 fadeHeight = (Sint16)(fadeRect.h * scaleMod);

    SDL_Rect fadeLocation = { (Sint16)(_player->GetTransform().position.x - fadeWidth / 2), (Sint16)(_player->GetTransform().position.y - fadeHeight / 2), fadeWidth, fadeHeight };
    result = SDL_RenderCopy(renderer, _fadeImage, nullptr, &fadeLocation);

    fadeLocation = { (Sint16)(_opponent->GetTransform().position.x - fadeWidth / 2), (Sint16)(_opponent->GetTransform().position.y - fadeHeight / 2), fadeWidth, fadeHeight };
    result = SDL_RenderCopy(renderer, _fadeImage, nullptr, &fadeLocation);

    scaleMod = (_ballMovementBlend * 0.25f) + 0.75f;
    fadeWidth = (Sint16)(fadeRect.w * scaleMod);
    fadeHeight = (Sint16)(fadeRect.h * scaleMod);

    fadeLocation = { (Sint16)(_ball->GetTransform().position.x - fadeWidth / 2), (Sint16)(_ball->GetTransform().position.y - (Sint16)(_ball->GetTransform().position.z * Z_HEIGHT_CORRECTION) - fadeHeight / 2), fadeWidth, fadeHeight };
    result = SDL_RenderCopy(renderer, _fadeImage, nullptr, &fadeLocation);

    result = SDL_SetRenderTarget(renderer, previousTarget);
    SDL_SetRenderDrawColor(renderer, r, g, b, a);

    SDL_RenderCopy(renderer, _fadeSurface, nullptr, nullptr);
}

void TennisScreen::AddFoggedObject(std::shared_ptr<GameObject> target, std::function<bool()> movement, float minimum, float maximum)
{
    auto previousEntry = _foggedObjects.find(target);
    if (previousEntry != _foggedObjects.end())
    {
        previousEntry->second.currentFogAmount = MathUtils::Clamp(previousEntry->second.currentFogAmount, minimum, maximum);
        previousEntry->second.minimumFogAmount = minimum;
        previousEntry->second.maximumFogAmount = maximum;
        previousEntry->second.movement = movement;
    }
    else
    {
        FoggedEntry entry = { target, minimum, minimum, maximum, movement };
        _foggedObjects.emplace(std::pair<std::shared_ptr<GameObject>, FoggedEntry>(target, entry));
    }
}

void TennisScreen::RemoveFoggedObject(std::shared_ptr<GameObject> target)
{
    auto previousEntry = _foggedObjects.find(target);
    if (previousEntry != _foggedObjects.end())
    {
        _foggedObjects.erase(target);
    }
}

void TennisScreen::CalculateDrawOrder(std::vector<std::shared_ptr<GameObject>>& drawOrder)
{
    // SUPER HACK GARBAGE ALGO.
    drawOrder.clear();

    drawOrder.insert(drawOrder.begin(), _objects.begin(), _objects.end());

    std::sort(drawOrder.begin(),
              drawOrder.end(),
              [](std::shared_ptr<GameObject> first, std::shared_ptr<GameObject> second) -> bool
    {
        float firstOffset = first->GetTransform().position.y + (first->GetTransform().position.z * Z_HEIGHT_CORRECTION);
        float secondOffset = second->GetTransform().position.y + (second->GetTransform().position.z * Z_HEIGHT_CORRECTION);

        return firstOffset < secondOffset;
    });
}

bool TennisScreen::IsRoundOver()
{
    bool multipleBounces = _bounces >= MAX_BOUNCES;
    bool ballStopped = (fabsf(_ball->GetVelocity().x) <= BALL_STOPPED_EPSILON &&
                        fabsf(_ball->GetVelocity().y) <= BALL_STOPPED_EPSILON &&
                        fabsf(_ball->GetVelocity().z) <= BALL_STOPPED_EPSILON &&
                        _ball->IsOnGround() == true);
    bool result = _roundState == RoundState::PLAY &&
        (multipleBounces || _hitNet || ballStopped);
    return result;
}
