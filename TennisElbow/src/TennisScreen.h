#pragma once

#include <functional>
#include <map>
#include <memory>
#include <vector>
#include <MathUtils.h>

#include <SDL_gamecontroller.h>

#include <Screen.h>

class HumanPlayer;
class Opponent;
class Net;
class Ball;
class GameObject;
class DoubtBat;

class TennisScreen : public Screen
{
public:
    enum class RoundState
    {
        SERVING,
        PLAY,
        END,
    };

    enum class GameState
    {
        PLAY,
        PAUSED
    };

    TennisScreen(SDL_GameController* controller);
    ~TennisScreen();

    void Update(const SDL_Event& e, float dt);
    void Draw(SDL_Renderer* renderer, float dt);

    void OnEnter() { }
    void OnEnterTransitionDidFinish() { }
    void OnExit() { }
    void OnExitTransitionDidFinish() { }

    void AddFoggedObject(std::shared_ptr<GameObject> target, std::function<bool()> movement, float minimum, float maximum);
    void RemoveFoggedObject(std::shared_ptr<GameObject> target);

    void Clear() { }

protected:
    void InitializeImpl(SDL_Renderer* renderer);

    void HandleInput(const SDL_Event& evt);
    void Reset();
    void CalculateDrawOrder(std::vector<std::shared_ptr<GameObject>>& drawOrder);

    bool IsRoundOver();
    void DrawFoggedObjects(SDL_Renderer* renderer, const float& dt);

    std::shared_ptr<Opponent> _opponent;
    std::shared_ptr<HumanPlayer> _player;
    std::shared_ptr<DoubtBat> _bat;
    std::shared_ptr<Ball> _ball;
    std::shared_ptr<Net> _net;

    SDL_Texture* _fadeSurface;
    SDL_Texture* _courtImage;
    SDL_Texture* _fadeImage;

    SDL_BlendMode _fadeBlendMode;
    float _playerMovementBlend;
    float _ballMovementBlend;
    
    float _fogAmount;
    float _fogDirection;

    struct FoggedEntry
    {
        std::shared_ptr<GameObject> target;
        float currentFogAmount;
        float minimumFogAmount;
        float maximumFogAmount;
        
        std::function<bool()> movement;

 /*       []()->bool;   
        bool (*movement)();*/
    };

    std::map<std::shared_ptr<GameObject>, FoggedEntry> _foggedObjects;

    // Using the default member-wise initializer for our new struct.
    Vector2 _servingDirection;

    RoundState _roundState;
    GameState _gameState;

    bool _hitNet;
    Uint8 _bounces;

    Vector3 _hitPosition;
    Vector3 _hitVelocity;

    std::vector<std::shared_ptr<GameObject>> _objects;
    std::vector<Vector3> _impactPoints;

    SDL_GameController* _controller;

    Sint32 _screenWidth;
    Sint32 _screenHeight;
    float _timeScale;

    static const Vector3 c_UpperCourtStartingPosition;
    static const Vector3 c_LowerCourtStartingPosition;
    static const Vector3 c_NetPosition;
};
