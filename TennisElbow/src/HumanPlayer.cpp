#include "HumanPlayer.h"

#include <iostream>

#include <SDL.h>
#include <SDL_gamecontroller.h>
#include <SDL_image.h>

#include "GameConstants.h"

HumanPlayer::HumanPlayer()
    : Player()
{
}

HumanPlayer::~HumanPlayer()
{
    Player::~Player();
    _controller = nullptr;
}

void HumanPlayer::Update(const SDL_Event& evt, const float& dt)
{
    float xvalue = 0.0f;
    float yvalue = 0.0f;
    if (SDL_GameControllerGetAttached(_controller))
    {
        xvalue = (float)SDL_GameControllerGetAxis(_controller, SDL_CONTROLLER_AXIS_LEFTX) / (float)SHRT_MAX;
        yvalue = (float)SDL_GameControllerGetAxis(_controller, SDL_CONTROLLER_AXIS_LEFTY) / (float)SHRT_MAX;

        if (abs(xvalue) < CONTROLLER_DEADZONE)
        {
            xvalue = 0.0f;
        }

        if (abs(yvalue) < CONTROLLER_DEADZONE)
        {
            yvalue = 0.0f;
        }
    }
    else
    {
        const Uint8* states = SDL_GetKeyboardState(nullptr);
        xvalue = states[SDL_SCANCODE_A] || states[SDL_SCANCODE_LEFT]
                 ? -1.0f
                 : states[SDL_SCANCODE_D] || states[SDL_SCANCODE_RIGHT]
                   ? 1.0f
                   : 0.0f;

        yvalue = states[SDL_SCANCODE_W] || states[SDL_SCANCODE_UP]
                 ? -1.0f
                 : states[SDL_SCANCODE_S] || states[SDL_SCANCODE_DOWN]
                   ? 1.0f
                   : 0.0f;
    }

    _movement = { xvalue * _speed * dt, yvalue * _speed * dt };

    __super::Update(evt, dt);
}

void HumanPlayer::SetController(SDL_GameController* controller)
{
    _controller = controller;
}
