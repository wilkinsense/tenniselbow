#pragma once

#include <SDL_GameController.h>

#include "Player.h"

struct SDL_Texture;

class HumanPlayer : public Player
{
public:
    HumanPlayer();
    ~HumanPlayer();

    void Update(const SDL_Event& evt, const float& dt) override;

    void SetController(SDL_GameController* controller);

protected:
    SDL_GameController* _controller = nullptr;
};
#pragma once
