#pragma once

#include <memory>
#include "Player.h"

struct SDL_Texture;
class Ball;

class Opponent : public Player
{
public:
    Opponent();
    ~Opponent();

    void Update(const SDL_Event& evt, const float& dt) override;
    void Draw(SDL_Renderer* renderer, const float& dt) override;

    void CalculateBallImpact(std::shared_ptr<Ball> ball);
    void Reset();

protected:
    Vector2 _direction = Vector2::Zero;
    Vector2 _intendedImpactPoint = Vector2::Zero;
};
