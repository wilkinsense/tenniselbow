#pragma once

#include <GameObject.h>

struct SDL_Texture;
struct SDL_Rect;

class Net : public GameObject
{
public:
    Net();
    ~Net();

    void Initialize(SDL_Renderer* renderer) override;

    void Update(const SDL_Event& evt, const float& dt);
    void Draw(SDL_Renderer* renderer, const float& dt);

    void GetDrawRect(SDL_Rect* drawRect);

protected:
    SDL_Texture* _netImage = nullptr;
    int _width = 0, _height = 0;
};
