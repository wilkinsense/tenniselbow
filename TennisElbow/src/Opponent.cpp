#include "Opponent.h"

#include <stdio.h>

#include <SDL.h>
#include <SDL_image.h>

#include <MathConstants.h>

#include "Ball.h"
#include "GameConstants.h"

Opponent::Opponent() : Player()
{
    Reset();
}

Opponent::~Opponent()
{
}

void Opponent::Update(const SDL_Event& evt, const float &dt)
{
    if (_intendedImpactPoint.x != -1.0f)
    {
        _direction = _intendedImpactPoint - GetTransform().position;
        if (_direction.Magnitude() > 1.0f) {
            _direction = MathUtils::Normalize(_direction);
        }
    }
    else
    {
        _direction = Vector2::Zero;
    }

    _movement = _direction * _speed * dt;

    __super::Update(evt, dt);
}

void Opponent::Draw(SDL_Renderer* renderer, const float &dt)
{
    __super::Draw(renderer, dt);

    if (_intendedImpactPoint.x != -1.0f)
    {
        Uint8 r, g, b, a;
        SDL_GetRenderDrawColor(renderer, &r, &g, &b, &a);

        SDL_SetRenderDrawColor(renderer, 255, 0, 0, SDL_ALPHA_OPAQUE);

        SDL_Rect impactLocation = { (int)_intendedImpactPoint.x - (5), (int)_intendedImpactPoint.y - (5), 10, 10 };
        SDL_RenderDrawRect(renderer, &impactLocation);

        SDL_SetRenderDrawColor(renderer, r, g, b, a);
    }
}

void Opponent::Reset()
{
    _direction = Vector2::Zero;
    _intendedImpactPoint = { -1.0f, -1.0f };
}

void Opponent::CalculateBallImpact(std::shared_ptr<Ball> ball)
{
    Vector3 currentVelocity = ball->GetVelocity();
    Vector3 currentPosition = ball->GetTransform().position;

    float airtime = (fabsf((currentPosition.z + currentVelocity.z) / (Z_HEIGHT_CORRECTION + PHYSICS_GRAVITY)) / ACCURACY_CORRECTION);

    _intendedImpactPoint = {
      currentPosition.x + (currentVelocity.x * airtime),
      currentPosition.y + (currentVelocity.y * airtime)
    };
}
