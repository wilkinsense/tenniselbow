#include "DoubtBat.h"

#include "GameConstants.h"

DoubtBat::DoubtBat()
    : Player("res/doubtbat.png",
             BAT_SPEED)
{
}

DoubtBat::~DoubtBat()
{
    SetTarget(nullptr);
}

void DoubtBat::Update(const SDL_Event& evt, const float& dt)
{
    if (_target)
    {
        Vector2 difference = _target->GetTransform().position - GetTransform().position;

        bool willFollowTarget = difference.Magnitude() > 10;

        if (willFollowTarget && !_followingTarget)
        {
            bool flipHorizontal = (rand() % 10) >= 5;
            difference *= Vector2(flipHorizontal ? -1.0f : 1.0f, flipHorizontal ? 1.0f : -1.0f);
        }

        _followingTarget = willFollowTarget;

        if (_followingTarget)
        {
            _direction = MathUtils::Normalize<Vector2>(difference);
        }
        /*if (_direction.Magnitude() > 1.0f) {
            _direction = MathUtils::Normalize(_direction);
        }*/
    }
    else
    {
        _direction = Vector2::Zero;
    }

    _movement = _direction * _speed * dt;

    Player::Update(evt, dt);
}

void DoubtBat::SetTarget(std::shared_ptr<GameObject> target)
{
    _target = target;
}
