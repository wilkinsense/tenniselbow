#pragma once

#include <GameObject.h>
#include <MathUtils.h>

struct SDL_Texture;
struct SDL_Rect;

class Ball : public GameObject
{
public:
    Ball();
    ~Ball();

    void Initialize(SDL_Renderer* renderer) override;

    void Update(const SDL_Event& evt, const float& dt) override;
    void Draw(SDL_Renderer* renderer, const float& dt) override;

    void ApplyForce(const Vector3 &force);
    void SetActive(const bool &active);
    const bool GetActive() const;

    const bool IsOnGround() const;
    const bool IsBouncing() const;
    const Vector3& GetVelocity();

    void TEST_SetVelocity(Vector3 velocity);

    void Reset();

    void GetDrawRect(SDL_Rect* drawRect);
    void GetShadowDrawRect(SDL_Rect* shadowDrawRect);

protected:
    SDL_Texture* _ballImage = nullptr;
    SDL_Texture* _shadowImage = nullptr;

    Vector3 _velocity = Vector3::Zero;
    float _mass = 0.0f;
    bool _onGround = false;
    bool _bouncing = false;
    bool _active = false;

    int _width = 0, _height = 0;
};
