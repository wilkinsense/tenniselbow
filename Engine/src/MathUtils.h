#pragma once

// A structure to hold two floats. Similar to SDL_Point (though it contains ints).
struct Vector2
{
public:
    float x = 0.0f;
    float y = 0.0f;

    Vector2();
    Vector2(float x, float y);

    static const Vector2 Zero;
    Vector2 operator+(const Vector2& rhs);
    Vector2 operator-(const Vector2& rhs);
    Vector2 operator+=(const Vector2& rhs);
    Vector2 operator-=(const Vector2& rhs);
    Vector2 operator*(const float& rhs);
    Vector2 operator/(const float& rhs);
    Vector2 operator*(const Vector2& rhs);
    Vector2 operator/(const Vector2& rhs);
    Vector2 operator*=(const float& rhs);
    Vector2 operator/=(const float& rhs);
    Vector2 operator*=(const Vector2& rhs);
    Vector2 operator/=(const Vector2& rhs);

    virtual const float Magnitude() const;
};

struct Vector3 : public Vector2
{
public:
    float z = 0.0f;

    Vector3();
    Vector3(float x, float y, float z);

    static const Vector3 Zero;
    Vector3 operator+(const Vector3& rhs);
    Vector3 operator+(const Vector2& rhs);
    Vector3 operator-(const Vector3& rhs);
    Vector3 operator-(const Vector2& rhs);
    Vector3 operator+=(const Vector3& rhs);
    Vector3 operator+=(const Vector2& rhs);
    Vector3 operator*(const float& rhs);
    Vector3 operator/(const float& rhs);

    virtual const float Magnitude() const override;
};

struct Vector4 : public Vector3
{
public:
    float w = 0.0f;

    Vector4();
    Vector4(float x, float y, float z, float w);

    static const Vector4 Zero;
    Vector4 operator+(const Vector4& rhs);
    Vector4 operator-(const Vector4& rhs);
    Vector4 operator+=(const Vector4& rhs);
    Vector4 operator*(const float& rhs);
    Vector4 operator/(const float& rhs);

    virtual const float Magnitude() const override;
};

struct Transform
{
public:
    Vector3 position = Vector3::Zero;
    Vector3 rotation = Vector3::Zero;
    Vector3 scale = Vector3::Zero;
};

class MathUtils
{
public:
    static float ToRadians(float degrees);
    static float ToDegrees(float radians);

    template<typename Vector>
    static Vector Normalize(Vector vector) {
        return vector / vector.Magnitude();
    }

    static float Clamp(float value, float min, float max);
};
