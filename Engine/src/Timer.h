#pragma once

class Timer;

typedef void(Timer::* TimerInstanceEvent)();
typedef void(*TimerEvent)();

class Timer
{
public:
    enum class TimerState
    {
        STOPPED,
        RUNNING,
        PAUSED
    };

    Timer();

    void Update();

    void Start();
    void Pause();
    void Stop();
    void Reset();

    float GetElapsedTime();
    float GetDeltaTime();
    float GetDuration();

    void SetDuration(float duration);

    void SetTimerEvent(TimerEvent evt);
    void SetTimerEvent(TimerInstanceEvent evt);

protected:
    float _oldTime = 0.0f, _currentTime = 0.0f, _deltaTime = 0.0f;
    float _duration = 0.0f, _elapsedTime = 0.0f;
    bool _paused = false;

    TimerState _state = TimerState::STOPPED;
    TimerEvent _timerEvt;
    TimerInstanceEvent _timerInstEvt;
};
